#!/bin/bash
set -e

USER_ID=${USER_ID:-'650446'}
LICENSE_KEY=${LICENSE_KEY:-'8kIavgc90UCmDdGs'}
PRODUCT_IDS=${PRODUCT_IDS:-'GeoLite2-City'}

cat > /etc/GeoIP.conf <<EOL
# The following UserId and LicenseKey are required placeholders:
UserId $USER_ID
LicenseKey $LICENSE_KEY
ProductIds $PRODUCT_IDS
EOL

geoipupdate -d /mmdbshareVolume

if [ "$1" = 'cron' ]; then
    CRONTAB_MAILTO=${CRONTAB_MAILTO:-}
    CRONTAB_FREQUENCY=${CRONTAB_FREQUENCY:-'* * * * *'}

    cat > ~/crontab <<EOL
# top of crontab
$CRONTAB_FREQUENCY /usr/bin/geoipupdate  > ./geoipupdate.log 
# end of crontab
EOL
    crontab ~/crontab
    cron -f
fi

exec "$@"